﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player_Mov : MonoBehaviour
{
    // Start is called before the first frame update

    public AudioSource jump_sound;
    public float speed;
    public float jumpHeight;
    private Rigidbody2D player;
    private bool isJumping = false;

    

    void Start()
    {
        player = GetComponent<Rigidbody2D> ();
        jump_sound = GetComponent<AudioSource> ();

    }

    // Update is called once per frame
    void Update()
    {

        
        
        //Horizontal
        float moveHorizontal = Input.GetAxis ("Horizontal");


        //Vector2D para el movimiento con la componente horizontal
        Vector2 movement = new Vector2 (moveHorizontal, 0);

        //fuerza calculada a partir del movimiento y la velocidad
        
        if (Input.GetKeyDown(KeyCode.Space) && !isJumping) // pulsa espacio + no estas en el aire
        { 
            player.AddForce(Vector2.up * jumpHeight, ForceMode2D.Impulse); 
            isJumping = true; // estas en el aire, por lo que no puedes volver a saltar
            jump_sound.Play();
        } 

        
        player.AddForce (movement * speed);


    }

    void OnCollisionEnter2D (Collision2D col)
    {
        if (col.collider.tag == "Ground" || col.collider.tag == "Pipe")
        { 
            isJumping = false; // de esta forma no salta cuando esta en el aire, solo cuando toque el suelo o una tuberia
        }

        if(col.collider.tag == "Enemy" || col.collider.tag == "Void") // si toca a un enemigo o el vacio (debajo del escenario) saldrá la pantalla de GameOver
        {      
            GameOver();
        }

        if(col.collider.tag == "Flag")// si toca a un enemigo o el vacio (debajo del escenario) saldrá la pantalla de GameOver
        {
            Win();
        }
        
    }


    public void GameOver() // Llamada a la escena de GameOver
    {
        SceneManager.LoadScene("GameOver");
    }
    public void Win() // Llamada a la escena de GameOver
    {
        SceneManager.LoadScene("Win");
    }


}
