﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Mov : MonoBehaviour
{

    private Rigidbody2D goomba;
    public float enemy_speed;

    //float movHor = -1;

    Vector2 enemy_movement = new Vector2 (-1, 0); // inicialmente el enemigo se mueve hacia la izquierda


    // Start is called before the first frame update
    void Start()
    {
        goomba = GetComponent<Rigidbody2D> ();
    }

    // Update is called once per frame
    void Update()
    {
        
        goomba.AddForce (enemy_movement * enemy_speed);
    }


    void OnCollisionEnter2D (Collision2D col)
    {
        if (col.collider.tag == "Pipe") // al tocar una tuberia, cambia el sentido de su dirección multiplicando el vector de movimiento por -1
        { 
            enemy_movement = enemy_movement * -1;
        }

        
    }

}
