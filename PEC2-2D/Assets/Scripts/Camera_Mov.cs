﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Mov : MonoBehaviour
{
    public GameObject player;        //Public variable to store a reference to the player game object
    public float pos_Y;
    public float pos_Z;
    private Vector3 offset;            //Private variable to store the offset distance between the player and camera

    // Use this for initialization
    void Start () 
    {
        //Diferencia entre la posicion de la camara y la posicion del jugador
        offset = transform.position - player.transform.position;
    }

    // LateUpdate is called after Update each frame
    void LateUpdate () 
    {
        // Vamos sumando la diferencia a la componente X del jugador, de forma que Y y Z son constantes y seguimos al jugador lateralmente
        float pos_X = player.transform.position.x + offset.x;
        transform.position = new Vector3(pos_X,pos_Y,pos_Z);
    }
}
