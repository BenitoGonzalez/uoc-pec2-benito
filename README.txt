Para este nivel de Mario creado.

Se han creado 3 escenas:

- MainGame: que es el nivel como tal
- GameOver: Una pantalla de Game Over con un boton para reiniciar el nivel
- Win: Cuando se alcanza la bandera sale una pantalla diciendo que hemos ganado y nos da la opcion de repetir.

Mario se mueve con lo botones A y D para moverse a izquierda y derecha.
Para saltar, pulsar la barra espaciadora.

Los enemigos estan taggeados como "Enemy", si Mario colisiona con un objeto taggeado como enemigo, se cambiará a la escena GameOver y donde sonará la música de derrota.
Lo mismo ocurrirá si Mario cae al vacio, ya que hay un GameObject taggeado como "Void" con un BoxCollider que ocupa todo el escenario pero por debajo, si Mario lo toca también será Game Over.

Si Mario llega y toca la bandera del final, se cambiará la escena a Win y sonará la música de victoria asociada a esa escena (en concreto al canvas).

Los enemigos se mueven, en este caso se mueven en tramos entre tuberias, de fomra que cuando colisionan con una tuberia cambian el sentido de su movimeinto para ir de izquierda a dercha todo el rato.
